#pragma once
#include "opencv2/core/core.hpp"
#include "opencv2/video/video.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include <iostream>
#include <cassert>

using namespace System::Runtime::CompilerServices;
using namespace System::Drawing;


using namespace System;

namespace CppCLI {
	public ref class NativeClass sealed abstract
	{
	public:
		static int TestFunction() { return 42; }
	};
}
